Rails.application.routes.draw do
  get 'sessions/new'

  get 'users/new'

  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup', to: 'users#new'
  post '/signup',  to: 'users#create'
  
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  resources :users
  
  #get  '/movies',    to: 'movies#index', as: "movies"
  #get  '/movies/new', to: 'movies#new'
  #post '/movies/create', to: 'movies#create'
  #get 'movies/:id', to: 'movies#show'
  #get "movies/:id/edit", to: "movies#edit"
  #patch '/movies/:id', to: "movies#update"
  #delete '/movies/:id', to: "movies#destroy"
  
  #equivalent to using all db resources
  resources :movies
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #root 'application#hello'
  
  get '/actors', to: 'actors#index'
  get '/actors/new', to: 'actors#new'
  post '/actors/create', to: 'actors#create'
  get 'actors/:id', to: 'actors#show'
  get "actors/:id/edit", to: 'actors#edit'
  patch '/actors/:id', to: "actors#update"
  
  get  '/rolodex',    to: 'rolodex#index'
  get '/rolodex/new', to: 'rolodex#new'
  post '/rolodex/create', to: 'rolodex#create'
  get 'rolodex/:id', to: 'rolodex#show'
  get "rolodex/:id/edit", to: 'rolodex#edit'
  patch '/rolodex/:id', to: "rolodex#update"
  delete '/rolodex/:id', to: "rolodex#destroy"
  
end