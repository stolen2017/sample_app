class MoviesController < ApplicationController
  
  def index
    #matching_string = '%'
    if params[:query].present?
      matching_string = '%' + params[:query] + '%' 
      @movies = Movie.where("title LIKE ? OR year_released LIKE ?", matching_string, matching_string)
    else
      @movies = Movie.all.order(id: :desc)
    end
    
    #@movies = Movie.where("title LIKE '#{params[:start]}%'").order(id: :desc)
    
    #@movies = Movie.all.order(id: :desc)
    
    #@movies = Movie.where("title LIKE ?", "#{params[:start]}%").order(id: :desc)
    
    
    #@count = Movie.count
  end
  
  def new
    @movie = Movie.new
  end
  
  def create
    @movie = Movie.create(
      #title: params[:title],
      #genre: params[:genre], 
      #year_released: params[:year_released], 
      #info_url: params[:info_url]

      #title: params[:movie][:title],
      #genre: params[:movie][:genre], 
      #year_released: params[:movie][:year_released], 
      #info_url: params[:movie][:info_url]
      params.require(:movie).permit(:title, :genre, :year_released, :info_url)
    )
  end
  
  def show
    @movie = Movie.find(params[:id])
  end
  
  def edit
    @movie = Movie.find(params[:id])
  end
  
  def update
    #@movie = Movie.find(params[:id])  
    #@movie.title = params[:title]
    #@movie.genre = params[:genre]
    #@movie.year_released = params[:year_released]
    #@movie.info_url = params[:info_url]
    #@movie.save
    
    @movie = Movie.find(params[:id])  
    #@movie.title = params[:movie][:title]
    #@movie.genre = params[:movie][:genre]
    #@movie.year_released = params[:movie][:year_released]
    #@movie.info_url = params[:movie][:info_url]
    #@movie.save
    
    @movie.update_attributes(
      params.require(:movie).permit(:title, :genre, :year_released, :info_url)
    )
    #redirect_to "/movies"
    redirect_to movies_path
  end
  
  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    #redirect_to "/movies".
    redirect_to movies_path
  end
end
