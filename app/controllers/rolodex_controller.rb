class RolodexController < ApplicationController
  def index
    #@rolodexes = Rolodex.all
    
    if params[:query].present?
      matching_string = '%' + params[:query] + '%' 
      @rolodexes = Rolodex.where("first_name LIKE ? OR last_name LIKE ?", matching_string, matching_string)
    else
      @rolodexes = Rolodex.all
    end
  end
  
  def new
    @rolodex = Rolodex.new
  end
  
  def create
    @rolodex = Rolodex.create(first_name: params[:first_name],last_name: params[:last_name], phone_number: params[:phone_number], email: params[:email])
  end
  
  def show
    @rolodex = Rolodex.find(params[:id])
  end
  
  def edit
    @rolodex = Rolodex.find(params[:id])
  end
  
  def update
    @rolodex = Rolodex.find(params[:id])  
    @rolodex.first_name = params[:first_name]
    @rolodex.last_name = params[:last_name]
    @rolodex.phone_number = params[:phone_number]
    @rolodex.email = params[:email]
    #@rolodex.first_name = params[:rolodex][:first_name]
    #@rolodex.last_name = params[:rolodex][:last_name]
    #@rolodex.phone_number = params[:rolodex][:phone_number]
    #@rolodex.email = params[:rolodex][:email]
    
    @rolodex.save
    
    redirect_to rolodex_path
  end
  
  def destroy
    @rolodex = Rolodex.find(params[:id])
    @rolodex.destroy
    #redirect_to "/movies".
    redirect_to rolodex_path
  end
end
