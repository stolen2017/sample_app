class ActorsController < ApplicationController
  
  def index
    @actors = Actor.all.order(id: :desc)
  end
  
  def new
    @actor = Actor.new
  end
  
  def create
    @actor = Actor.create(name: params[:name],gender: params[:gender], zodiac: params[:zodiac])
  end
  
  def show
    @actor = Actor.find(params[:id])
  end
  
  def edit
    @actor = Actor.find(params[:id])
  end
  
  def update
    @actor = Actor.find(params[:id])  
    @actor.name = params[:name]
    @actor.gender = params[:gender]
    
        
    #@actor = Actor.find(params[:id])  
    #@actor.title = params[:actor][:name]
    #@actor.genre = params[:actor][:gender]
    
    @actor.save
    #@actor.update_attributes(
      #params.require(:actor).permit(:name, :gender)
    #)
  end
end
